changeService();
changeWorkCategory();
changeSlide();


function changeService(){

  let serviceMenu = document.getElementsByClassName('services-menu')[0];
  let servicesItems = document.getElementsByClassName('services-item');
  let serviceDescription = document.getElementsByClassName('service-description');

  serviceMenu.addEventListener('click', () => {
    tabChange(servicesItems, 'triangle', serviceDescription, 'active-flex', 'shown', 'hidden');
  })

}

function tabChange(item, itemChangingClass, content, active, shown, hidden) {

  for (let i = 0; i < item.length; i++) {
    item[i].classList.remove(itemChangingClass);
    event.target.classList.add(itemChangingClass);
  }

  for (let i = 0; i < content.length; i++) {
    content[i].classList.remove(active);
    content[i].classList.remove(shown);
    content[i].classList.add(hidden);

    if (content[i].getAttribute('data-category') == event.target.getAttribute('data-category') || (event.target.getAttribute('data-category') == 'all' && content[i].getAttribute('data-status') == 'visible')) {
      content[i].classList.add(active);
      setTimeout( () => {
        content[i].classList.remove(hidden);
        content[i].classList.add(shown);
      },10)
    }

  }

}

function loadMoreContent(elem, number){
  let className = elem.getAttribute('data-target');
  let target = document.getElementsByClassName(className);
  let targetNotActive = [];

  for (let element of target) {
    if (element.classList.contains('active') == false) {
      targetNotActive.push(element);
    }
  }

  for (let i = 0; i < number; i++) {
    targetNotActive[i].classList.add('active');
    targetNotActive[i].classList.add('hidden');
     setTimeout(() => {
      targetNotActive[i].classList.remove('hidden');
      targetNotActive[i].classList.add('shown');
        },10)
  }

  if (targetNotActive[targetNotActive.length-1].classList.contains('active')) {
    elem.classList.add('disabled');
  }
}

function changeWorkCategory(){
  let photos = document.getElementsByClassName('amazing-photo');
  let menu = document.getElementsByClassName('amazing-menu')[0];
  let menuItem = document.getElementsByClassName('amazing-menu-item')
  let loadMoreButton = document.getElementsByClassName('load-more')[0];

  menu.addEventListener('click', () => {
    loadMoreButton.classList.add('disabled');

    if (event.target.getAttribute('data-category') == 'all') {
      loadMoreButton.classList.remove('disabled');
    }

    tabChange(menuItem,'amazing-choosed-item', photos, 'active', 'shown', 'hidden');   
  })

}


function changeSlide() {
  let leftArrow = document.getElementsByClassName('arrow')[0];
  let rightArrow = document.getElementsByClassName('arrow')[1];
  let counter = 3;
  let slides = document.getElementsByClassName('slide');
  let images = document.getElementsByClassName('choose-photo');
  let changeSlideContainer = document.getElementsByClassName('choose-slide-container')[0];
  let num = 0;
  let prev = counter-1;
  
  
  leftArrow.addEventListener('click', () => {

    if(counter == 0){
      counter = slides.length-1;
      prev = counter-1
    } else {
      counter--;
      prev--
    }
     
    if(counter == 0){
      prev = slides.length-1;
    }
      
    for (let i = 0; i<slides.length; i++) {
      if (slides[i].classList.contains('active-slide') == false) {
        slides[i].classList.remove('right-slide')
        slides[i].classList.add('left-slide')
      }
    }

    slides[counter].classList.remove('active-slide');
    slides[counter].classList.add('right-slide');
        
    slides[prev].classList.add('active-slide');
    slides[prev].classList.remove('left-slide');
    slides[prev].classList.remove('right-slide');
  
    images[counter].classList.remove('active-photo');
    images[prev].classList.add('active-photo');
       
  })
  
  rightArrow.addEventListener('click', () => {

    for (let i = 0; i<slides.length; i++) {
      if (slides[i].classList.contains('active-slide') == false) {
        slides[i].classList.remove('left-slide')
        slides[i].classList.add('right-slide')
      }
    }

    if (counter == slides.length) {
      counter = 0;
    }
      
    slides[prev].classList.remove('active-slide');
    slides[prev].classList.add('left-slide');
  
    slides[counter].classList.remove('right-slide')
    slides[counter].classList.add('active-slide');
  
    images[prev].classList.remove('active-photo');
    images[counter].classList.add('active-photo');
    counter++;
    prev++;

    if (prev == slides.length) {
      prev = 0;
    }
      
  })

  changeSlideContainer.addEventListener('click', () => {

    for (let i = 0; i < images.length; i++) {
      if (images[i] == event.target) {
        num = i;
        counter = i+1;
        prev = counter - 1;
        
        for (let i = 0; i < images.length; i++) {

          if (slides[i].classList.contains('active-slide')) {
            slides[i].classList.remove('active-slide');
          }

          if (images[i].classList.contains('active-photo')) {
            images[i].classList.remove('active-photo');
          }

        }

        for (let i = 0; i < num; i++) {
          slides[i].classList.remove('right-slide');
          slides[i].classList.add('left-slide');
        }

        for (let i = images.length-1; i > num; i--) {
          slides[i].classList.remove('left-slide')
          slides[i].classList.add('right-slide');
        }

        slides[num].classList.remove('left-slide');
        slides[num].classList.remove('right-slide');
    
        slides[num].classList.add('active-slide');
        images[num].classList.add('active-photo');
      }
    }
  })
}

